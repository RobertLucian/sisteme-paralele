import math
from mpi4py import MPI
from random import randint
from time import time

# also check sum_n_numbers.jpg photo

def main():
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()


    # here we're gonna have to check if size is 2^n
    exponent = int(math.log2(size))
    if 2 ** exponent != size:
        print('there must be 2^n processes')

    if rank == 0:

        # generate lots of random numbers
        v_size = 1000000
        vector = [randint(0, 10) for x in range(v_size)]
        # and split them by the number of processes
        chunk_size = int(v_size / size)
        leftover_size = v_size - chunk_size * size
        
        start = time()
        # send to each leaf node each chunk of numbers 
        for i in range(1, size - 1):
            comm.send(vector[chunk_size * i: chunk_size * (i + 1)], i)
        comm.send(vector[-(chunk_size + leftover_size):], size - 1)
        
        # calculate the sum for the given chunk on node 0
        no_sum = sum(vector[0:chunk_size])

        # and receive the sums from all corresponding nodes (1, 2, 4, 8, 16, etc)
        # can also check the printed photo of my notes
        for exp in range(exponent):
            receiving_node = 2 ** exp
            no_sum += comm.recv(source=receiving_node)
        end = time()

        # and display the calculated sum from all nodes
        # and also a real one to compare it with, which is done sequentially
        print("Calculated ", no_sum, 'with', int((end - start) * 1000), 'milliseconds')

        start = time()
        no_sum = sum(vector)
        end = time()
        print("Real ", sum(vector), 'with', int((end - start) * 1000), 'milliseconds')

    else:
        # receive chunk of numbers from node 0
        # and calculate the sum
        data = comm.recv(source = 0)
        suma = sum(data)

        # send the calculated sum from all odd nodes
        # to all nodes with an index higher by 1
        # this is only done on the leaf nodes
        if rank % 2 == 1:
            comm.send(suma, rank - 1)

        # for every level in the graph (starting with the leafs)
        for exp in range(exponent):
            # calculate the node from which it has to receive data
            upper_node = rank + 2 ** (exp - 1)
            multiple = 2 ** exp
            
            # if the nodes are a multiple of 2 ^ level of the graph
            # and the nodes are not leafs
            if rank % multiple == 0 and multiple > 1:
                # check if we have type A of nodes (those that have to receive and send)
                if ((rank - multiple) / (multiple)) % 2 == 0:
                    lower_node = rank - multiple
                    # print(rank, lower_node, upper_node, exp, 'a')
                    suma += comm.recv(source=upper_node)
                    comm.send(suma, lower_node)
                # otherwise just receive from another node and keep
                # the value in this current node
                else:
                    # print(rank, rank, upper_node, exp, 'b')
                    suma += comm.recv(source=upper_node)

if __name__ == "__main__":
    main()
