from mpi4py import MPI
from numpy import zeros, array, dot, int64
from numpy.random import randint
from time import time

def main():

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()

    if rank == 0:
        A = array([
            [1, 2, 1, 1, 1, 2],
            [0, 2, 1, 1, 1, 2],
            [0, 0, 1, 3, 1, 2],
            [0, 0, 0, 3, 1, 2],
            [0, 0, 0, 0, 1, 2],
            [1, 2, 1, 1, 1, 2]
        ])  
        B = array([
            [1, 0, 0, 0, 0, 2],
            [0, 2, 0, 0, 0, 2],
            [0, 0, 8, 0, 0, 2],
            [0, 0, 0, 5, 0, 2],
            [0, 0, 0, 0, 1, 2],
            [0, 0, 0, 0, 0, 2]
        ])
        matrix_size = 6
        if size < 3:
            print('too few processes. There has to be more than 2 processes.')
            return
        if size - 1 != matrix_size:
            print('the number of processes doesn\'t match the matrix size')
            return

    if rank == 0:
        start = time()

        diag = B.diagonal()
        for i in range(1, size):
            data = (A[i - 1, :], diag, B[:, -1])
            comm.send(data, i)

        C = zeros(shape=(size - 1, size - 1), dtype=int64)
        for i in range(1, size):
            C[i - 1, :] = comm.recv(source = i)

        end = time()

        print(C)
        print('Spent {:.2f} milliseconds'.format((end - start) * 1000))
        # should get the following matrix for 7 processes
        # [[ 1  4  8  5  1 16]
        #  [ 0  4  8  5  1 14]
        #  [ 0  0  8 15  1 14]
        #  [ 0  0  0 15  1 12]
        #  [ 0  0  0  0  1  6]
        #  [ 1  4  8  5  1 16]]
    else:
        # save the row, diag and the last row in matrix B
        row, diag, lc = comm.recv(source=0)
        n = row.shape[0]
        idx = rank - 1
        new = zeros(n)

        for i in range(n):
            final_idx = (i + idx) % n
            if final_idx != n - 1:
                new[final_idx] = row[final_idx] * diag[final_idx]
            else:
                new[final_idx] = dot(row, lc)
            # if rank == 1:
            #     comm.send(col, size - 1)
            # else:
            #     comm.send(col, rank - 1)
            # if rank == size - 1:
            #     col = comm.recv(source=1)
            # else:
            #     col = comm.recv(source=rank + 1)

        comm.send(new, 0)

if __name__ == "__main__":
    main()