from mpi4py import MPI
import math

def main():
    # initialize
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()
    
    # row/column compute functions
    row_compute = lambda rank: math.floor((-1 + math.sqrt(1 + 8 * rank)) / 2)
    col_compute = lambda rank, row: int(rank - row * (row + 1) / 2)
    to_rank = lambda row, col: (row * (row + 1) / 2 + col)

    printer = lambda row, col, data: print("row: {}, column: {}, msg: {}".format(row, col, data))

    # calculate rows/columns
    max_row = row_compute(size - 1) + 1
    max_col = col_compute(size - 1, max_row - 1) + 1
    row = row_compute(rank)
    col = col_compute(rank, row)

    # check if we've got the right number of processes
    if max_col != max_row:
        if row == 0 and col == 0:
            print('Wrong number of processes. Use n(n+1)/2 processes.')
        return

    # do the whole broadcasting shit
    if row == 0 and col == 0:
        message = "Hello Distributed Computing"
        print(max_row, max_col, size)
        comm.send(message, to_rank(row + 1, col))

        printer(row, col, message)
    else:
        # if we're in the lift
        if col == 0:
            data = comm.recv(source=to_rank(row - 1, col))
            printer(row, col, data)

            # send down the lift if there are still available nodes
            if row < max_row - 1:
                comm.send(data, to_rank(row + 1, col))

            # send to the right
            if row > 0:
                # for the bloody row = 1
                comm.send(data, to_rank(row, col + 1))
            if row > 1:
                # and to the left if row = 2, 3, ...
                comm.send(data, to_rank(row, row))             
        else:
            if row == 1:    
                data = comm.recv(source=to_rank(row, col - 1))

                printer(row, col, data)
            elif col < (row + 1) / 2: # if we're on the lefthand side
                # get from the left and send if there's still anything
                data = comm.recv(source=to_rank(row, col - 1))
                # and send to the right if there are still available nodes
                if col < (row + 1) / 2 - 1:
                    comm.send(data, to_rank(row, col + 1))

                printer(row, col, data)

            else:
                # receive from the right
                if row == col:
                    data = comm.recv(source=to_rank(row, 0))
                    printer(row, col, data)
                else:
                    data = comm.recv(source=to_rank(row, col + 1))

                # and then propagate to the left
                if col > (row + 1) / 2:
                    comm.send(data, to_rank(row, col - 1))

                printer(row, col, data)
                

if __name__ == "__main__":
    main()